Scaling to Petascale Institute - Cori Quickstart
================================================

For a quick test of your training account, after `Logging in`_ jump down to
`Run a test job:`_ and follow the instructions there.

Cori Overview
-------------

Cori is a Cray XC40 with:

- Aries Dragonfly-topology network
- 9688 Intel Xeon Phi "KNL" nodes each with: 

  - 68 cores
  - 4 hyperthreads/core
  - 16GB MCDRAM
  - 96GB DDR RAM

- 2388 Intel Xeon "Haswell" nodes each with: 

  - 2 sockets
  - 16 cores/socket
  - 2 hyperthreads/core
  - 128GB DDR RAM

- 28PB Lustre PFS, accessible as $SCRATCH

See http://www.nersc.gov/users/computational-systems/cori/configuration/ for 
further details.


Logging in
----------

Login with ssh::

  $ ssh -l my_login_name cori.nersc.gov

For exercises needing a GUI display you can start an X server on your 
workstation (try Xming_ for windows or XQuartz_ for Mac) and login with::

  $ ssh -X -l my_login_name cori.nersc.gov

Non-local X connections are notoriously slow, so you will probably get better 
usability by logging in via NX_. You will need to first install the NX client 
on your workstation.

.. _Xming: http://www.straightrunning.com/XmingNotes/
.. _XQuartz: https://www.xquartz.org/
.. _NX: http://www.nersc.gov/users/connecting-to-nersc/using-nx/

A good way to test if you have a working X setup is to run ``xterm`` from the 
Cori prompt. A small terminal window should appear on your screen.

See http://www.nersc.gov/users/connecting-to-nersc/ for further details.


Filesystems
-----------

``$HOME`` is intended for source code, configuration files, etc, **NOT** for 
running jobs
``$SCRATCH`` is a large, fast Lustre filesystem for short-term storage (files 
are subject to purging after 8 weeks). This is the best place to run most jobs 
``/project`` is for medium-term storage of files shared amongst memobers of a 
project. A copy of the traiing exercises will be held on ``/project``

See http://www.nersc.gov/users/storage-and-file-systems/ for further details.


Environment modules
-------------------

Software on Cori is managed through Environment modules. To see a list of 
available software use ``module avail``. Loading an unloading modules updates
you PATH and other environment variable to make the software available for use.
To see exactly what a module does use ``module show some_module_name``. To 
add a module to your environment use ``module load some_module_name``. To 
remove it from your environment use ``module unload some_module_name``. To see 
which modules you currently have loaded uyse ``module list``.

We are providing a module named ``STPI-2017``, which sets the variable 
``$TRAINING`` to the location of the Scaling to Petascale Institute training
exercises. You can unpack these into your ``$HOME`` area with::

  $ module load STPI-2017
  $ cp -r $TRAINING $HOME/

See http://www.nersc.gov/users/software/nersc-user-environment/modules/ for 
further details.


Compiling code
--------------

Cray provides wrappers to support easily building MPI or non-MPI code for the 
Cray environment. These compiler wrappers are:

- ``CC`` for C++
- ``cc`` for C
- ``ftn`` for Fortran

The underlying compiler is set according to the ``PrgEnv-*`` module you have 
loaded. On Cori by default this is ``PrgEnv-intel``, providing the Intel suite
of compilers and MKL.

The Cray compielr wrappers build statically-linked binaries by default, if you
need dynamic linking add the ``-dynamic`` flag to your compile and link options.

See http://www.nersc.gov/users/computational-systems/cori/programming/compiling-codes-on-cori/ 
for further details.


Running Jobs
------------

Cori uses the Slurm batch system. A job script is a regular shell script 
decorated at the beginning of the script with ``#SBATCH`` directives. All 
directives must appear before any shell commands (comments and blank lines are 
ok), and anything specified in an ``#SBATCH`` directive may alteratively be 
specified as a command-line option.  

To start an interactive session via Slurm, jump down to `Interactive Jobs`_.

All batch jobs on Cori must specify:

- the number of nodes required (``#SBATCH -N 1``)
- the wallclock time required, in minutes (``#SBATCH -t 10``)
- the type of nodes required (``#SBATCH -C haswell`` or ``#SBATCH -C knl``).
  More on this below
- The filesystems required (``#SBATCH -L SCRATCH``)

Your job will be placed in the ``debug`` partition for faster queuing if it 
meets the constraints (up to 64 nodes and 30 minutes), otherwise it will go
to the ``regular`` partition

For KNL nodes you can specify the mode you require (default is ``quad,cache``)
with, eg ``#SBATCH -C knl,quad,flat``. Note that requesting nodes in a non-
default mode might require the system to reboot nodes, which will delay your job 
starting by about 30 minutes

The queues on Cori can be very long. If the training session has an associated 
reservation you can use it with ``#SBATCH --reservation=reservation_name``

Your batch script will be run on the first node allocated to your job, to run
a command on all nodes in the job you must use ``srun``. ``srun`` also performs 
the role of ``mpirun`` for running MPI jobs.

Slurm on Cori views each hyperthread as a CPU, and in some circumstances Unix 
processes are not distributed across the cores of a node in the way you might 
have expected, so we recommend explicitly reserving sufficient hyperthreads for
each ntask (via ``-c num_hyperthreads``) and explicitly binding your Slurm/MPI 
tasks to cores (via ``--cpu_bind=cores``). 

So a typical job script will look like::

  #!/bin/bash -l
  #SBATCH -N 2         #Use 2 nodes
  #SBATCH -t 00:30:00  #Set 30 minute time limit
  #SBATCH -L SCRATCH   #Job requires $SCRATCH file system
  #SBATCH -C knl,quad,cache   #Use KNL nodes

  # directives can be commented out: 
  ##SBATCH --reservation=STPI_cache

  mkdir -p $SCRATCH/exercise1/ex1.$SLURM_JOB_ID
  # after the first shell command, #SBATCH directives are ignored
  cd $SCRATCH/exercise1/ex1.$SLURM_JOB_ID

  srun -n 10 -c 4 --cpu_bind=cores $HOME/exercises/example1.exe > stdout 2>stderr

.. _Run a test job:

Run a test job:
^^^^^^^^^^^^^^^

You can submit a job with ``sbatch my_job_script.sh``. (We have an example job
script in this directory called ``cori-testjob.sh``). If your ``#SBATCH`` 
options are correct enough, you will see a message with your job ID::

  $ sbatch cori-testjob.sh
  Submitted batch job 5468709

You can check on the progress of your job in the queue with ``sqs``:: 

  $ sqs
  JOBID              ST   REASON       USER         NAME         NODES        USED         REQUESTED    SUBMIT                PARTITION    RANK_P       RANK_BF
  5468709            PD   Priority     sleak        cori-testjob 2            0:00         5:00         2017-06-22T11:25:20   debug        16943        N/A

If the ``ST`` field is ``PD``, your job is still in the queue. If it is ``R``, 
your job is running. If your job does not appear in the ``sqs`` output then
it has completed (hopefully successfully), and you should see a file in the
directory you submitted from like ``slurm-5468709.out``::

  $ sqs
  JOBID              ST   REASON       USER         NAME         NODES        USED         REQUESTED    SUBMIT                PARTITION    RANK_P       RANK_BF
  $ ls -lt
  total 12
  -rw-rw---- 1 sleak staff  172 Jun 22 11:34 slurm-5468709.out  

Take a look inside that file to see the output and any errors from your job::

  $ less -FX slurm-5468709.out
  running a simple command in /global/cscratch1/sd/sleak/test_my_account
  total 4
  -rw-rw---- 1 sleak sleak 115 Jun 22 11:34 stdout
  1: hello from nid00509
  2: hello from nid00509
  0: hello from nid00509
  4: hello from nid00521
  3: hello from nid00521

See http://www.nersc.gov/users/computational-systems/cori/running-jobs/ for 
further details.

Interactive Jobs
----------------

You can start an interactive job on Cori with ``salloc``. ``salloc`` takes (and 
requires) the same arguments as ``sbatch``:

- the number of nodes required (``-N 1``)
- the wallclock time required, in minutes (``-t 10``)
- the type of nodes required (``-C haswell`` or ``-C knl``).
  When using ``knl`` nodes you should also specify what mode you need the nodes 
  to be in, usually either ``-C knl,quad,cache`` or ``-C knl,quad,flat``
- The filesystems required (``-L SCRATCH``)
- In sessions with a reservation, specify the reservation with 
  ``--reservation=name_of_reservation``
- In sessions without a reservation, you might get a node faster by requesting
  ``--qos=interactive``. This requests nodes from a small set reserved for 
  interactive use, and if a node doesn't become available within 5 minutes the 
  salloc will be aborted

So to start an interactive session using KNL nodes in flat mode during the 
Thursday sessions (a reservation is set up for this day)::

  $ salloc -C knl,quad,flat --reservation=STPI_flat -N 1 -t 30 -L SCRATCH




