#!/bin/bash -l

function print_usage() {
    echo ""
    echo "Usage:"
    echo "      ./create_run_scripts.sh <matsize>"
    echo "          matsize:    MxN, with M and N as integers"
    echo ""
    echo "      Options:"
    echo "          -h | --help: Prints usage."
    echo ""
}

if [ $# -lt 1 ]
then
    echo "A matrix size is required."
    print_usage
    exit
fi

if [[ $1 == '-h' || $1 == '--help' ]]
then
    print_usage
    exit
fi

MATSIZE=$1
OUTFILE=run_jacobi-openacc_cpu_$MATSIZE.sh

echo "Creating run script for matrix size $1..."

echo "#!/bin/bash -l" > $OUTFILE
echo "#PBS -l nodes=1" >> $OUTFILE
echo "#PBS -q debug" >> $OUTFILE
echo "#PBS -l walltime=00:30:00" >> $OUTFILE
echo "#PBS -N openacc.prof.j.$MATSIZE" >> $OUTFILE
echo "#PBS -j oe" >> $OUTFILE
echo "#PBS -A stf007" >> $OUTFILE
echo "" >> $OUTFILE
echo "cd \$PBS_O_WORKDIR" >> $OUTFILE
echo "" >> $OUTFILE
echo "module load cudatoolkit" >> $OUTFILE
echo "module list" >> $OUTFILE
echo "" >> $OUTFILE
echo "export COMPUTE_PROFILE=1" >> $OUTFILE
echo "export COMPUTE_PROFILE_LOG=/lustre/atlas1/stf007/proj-shared/vgv/omp4cug/nvprof-logs/output.${MATSIZE}.\$HOSTNAME.%p" >> $OUTFILE
echo "" >> $OUTFILE
echo "INFILE=../inputs/input.$MATSIZE" >> $OUTFILE
echo "" >> $OUTFILE
echo "echo \"Jacobi OpenACC on GPU\"" >> $OUTFILE
echo "time -p aprun -n 1 ./j.openacc.cpu < \$INFILE" >> $OUTFILE
echo "" >> $OUTFILE
echo "echo \"\"" >> $OUTFILE
