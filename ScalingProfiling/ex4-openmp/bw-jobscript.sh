#!/bin/bash -l
#PBS -l nodes=1:ppn=32:xe
#PBS -l walltime=10:00
#PBS -j oe
#PBS -N ex4

# In this exercise, we will run our TAU-instrumented OpenMP version of miniFE
# with 1 MPI rank but a few OpenMP thread counts, and use TAU to identify issues
# in the OpenMP scaling

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $HOME/scratch/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}
SCRATCH=$HOME/scratch
. /opt/modules/default/init/bash
module swap PrgEnv-cray PrgEnv-intel
module load tau

cd $PBS_O_WORKDIR

# path to the miniFE executable we built:
ex=$HOME/$training_dir/ex4-openmp/build/miniFE-omp-tau.x

# A 150x150x150 miniFE job should finish within 5 minutes on a single core 
# of a BW node
sz=150
cmd="$ex -nx $sz -ny $sz -nz $sz"

# Each BlueWaters core has 2 hyperthreads, which PBS views as 2 CPUs, but 
# for this exercise we want each MPI task to have its own core. The following
# recipe calculates how many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cpus_per_node=$(lscpu | awk '/^CPU\(s\):/ {print $NF}')
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((cpus_per_node/cpus_per_task))
max_mpi_ranks=$((PBS_NUM_NODES * max_mpi_per_node))

# for OpenMP scaling, we'll only use 1 MPI rank:
nranks=1

# make the output of 'time' command easy to plot:
TIMEFORMAT=%R

# .. and prepare a gnuplot data file:
timing_file=$PBS_O_WORKDIR/timings-$PBS_JOBID.dat
echo "#nthreads" $'\t' "bm_time" $'\t' "walltime" >> $timing_file

# let's look at the profile with 1, 2, and 16 threads
for OMP_NUM_THREADS in 1 2 16 ; do
  export OMP_NUM_THREADS

  # CPU affinity is important with OpenMP:
  export OMP_PROC_BIND=true
  export OMP_PLACES=cores

  label=tau_profile-sz${sz}-${PBS_NUM_NODES}n-${OMP_NUM_THREADS}omp-$PBS_JOBID
  # run in a unique directory under $SCRATCH:
  rundir=$SCRATCH/$training_dir/ex4-openmp/$label
  mkdir -p $rundir
  cd $rundir

  tm=$( { time aprun -n $nranks $cmd > stdout 2> stderr ; } 2>&1 )

  # if the run succeeded, we should have a .yaml file in the $rundir with various
  # metrics about how the run went. We'll extract its report of the total run
  # time, and append a gnuplot data file with the timing info:
  bm_tm=$(awk -F: '/Total Program Time/ { print $2 }' *.yaml)
  echo "  $OMP_NUM_THREADS" $'\t' $bm_tm $'\t' $tm >> $timing_file

done
echo "finished miniFE runs at `date`"
