#!/bin/bash -l
## this exercise will potentially take some time - we'll be polite by
## using as few nodes as we can for as short time as we can in each job
## Select one at a time from the -l combinations:
#PBS -l nodes=1:ppn=32:xe,walltime=15:00
##PBS -l nodes=2:ppn=32:xe,walltime=10:00
##PBS -l nodes=4:ppn=32:xe,walltime=05:00
#PBS -l walltime=15:00
#PBS -j oe
#PBS -N ex5

# In this exercise we will run miniFE at a few different scales for each of a 
# few different sizes and plot the performance in terms of MFLOPS (Handily,
# miniFE reports a FLOP count for each run, based on the problem size)

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}
SCRATCH=$HOME/scratch
. /opt/modules/default/init/bash
module swap PrgEnv-cray PrgEnv-intel

cd $PBS_O_WORKDIR

# path to the miniFE executable we built at step 1:
ex=$HOME/$training_dir/ex1-scaling/build/miniFE.x

# Each BlueWaters core has 2 hyperthreads, which PBS views as 2 CPUs, but 
# for this exercise we want each MPI task to have its own core. The following
# recipe calculates how many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cpus_per_node=$(lscpu | awk '/^CPU\(s\):/ {print $NF}')
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((cpus_per_node/cpus_per_task))
max_mpi_ranks=$((PBS_NUM_NODES * max_mpi_per_node))

# make the output of 'time' command easy to plot:
TIMEFORMAT=%R

declare -a sizes=(100 150 200 300)
for j in $(seq 1 ${#sizes[@]} ); do
  sz=${sizes[$((j-1))]}
  # we'll make a separate gnuplot data file for each problem size:
  # this time we won't use $PBS_JOBID in the name, so multiple
  # jobs can append the same file:
  timing_file=$PBS_O_WORKDIR/sz${sz}-timings.dat
  if [[ ! -e $timing_file ]] ; then
    # write a heading line, if there isn't already one:
    echo "#nranks" $'\t' "flopcnt" $'\t' "walltime" $'\t' "bm_time" $'\t' "CG_MFlops" >> $timing_file
  fi

  cmd="$ex -nx $sz -ny $sz -nz $sz"

  # we'll start the bigger datasets at progressively higher core counts:
  for i in $(seq $j 10) ; do
    nranks=$((2**i)) # 1, 2, 4, 8, ...
    [[ $nranks -gt $max_mpi_ranks ]] && break # max we can scale to in this job
    nnodes=$(( (nranks+max_mpi_per_node-1)/max_mpi_per_node))
    # rather than holding 2 nodes while only using 1, we'll submit this job 
    # separately for each node count, and skip the runs that are not matched 
    # to the current node count:
    [[ $nnodes -lt $PBS_NUM_NODES ]] && continue

    label=sz${sz}-${PBS_NUM_NODES}n-${nranks}mpi-$PBS_JOBID

    # run in a unique directory under $SCRATCH:
    rundir=$SCRATCH/$training_dir/ex5-weakscaling/$label
    mkdir -p $rundir
    cd $rundir

    echo "starting miniFE sz=${sz} on with $nranks MPI processes at `date`"
    tm=$( { time aprun -n $nranks $cmd > stdout 2> stderr ; } 2>&1 )

    # find the flopcount and report CG Mflops in the yaml output:
    flopcnt=$(awk -F: '/Total CG Flops/ { print $2 }' *.yaml)
    cg_mflops=$(awk -F: '/Total CG Mflops/ { print $2 }' *.yaml)
    bm_tm=$(awk -F: '/Total Program Time/ { print $2 }' *.yaml)
    echo "  $nranks" $'\t' $flopcnt $'\t' $tm $'\t' $bm_tm $'\t' $cg_mflops >> $timing_file
  done
done
echo "finished miniFE runs at `date`"

