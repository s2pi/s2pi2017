#!/bin/bash -l
## this exercise will potentially take some time - we'll be polite by
## using as few nodes as we can for as short time as we can in each job
## Select one at a time from the -N -t combinations:
##PBS -l nodes=1:ppn=32:xe,walltime=15:00
#PBS -l nodes=2:ppn=32:xe,walltime=10:00
#PBS -l walltime=15:00
#PBS -j oe
#PBS -N ex7

# In this exercise, we will run an MPI+OpenMP version of miniFE at a few scales
# with differing balances of MPI and OpenMP tasks, to searhc for an optimal 
# configuration

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}

ex=$HOME/$training_dir/ex7-hybrid/build/miniFE-omp.x
# CPU affinity is important with OpenMP:
export OMP_PROC_BIND=true
export OMP_PLACES=cores

sz=150
cmd="$ex -nx $sz -ny $sz -nz $sz"

# Each BlueWaters core has 2 hyperthreads, which PBS views as 2 CPUs, but 
# for this exercise we want each MPI task to have its own core. The following
# recipe calculates how many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cpus_per_node=$(lscpu | awk '/^CPU\(s\):/ {print $NF}')
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((cpus_per_node/cpus_per_task))
max_mpi_ranks=$((PBS_NUM_NODES * max_mpi_per_node))

# make the output of 'time' command easy to plot:
TIMEFORMAT=%R

for OMP_NUM_THREADS in 1 2 4 8 ; do
  export OMP_NUM_THREADS
 
  # we'll use a different timing file for each threads-per-rank:
  # this time we won't use $SLURM_JOB_ID in the name, so multiple
  # jobs can append the same file:
  timing_file=$PBS_O_WORKDIR/timings-${OMP_NUM_THREADS}thr_per_rank.dat
  if [[ ! -e $timing_file ]] ; then
    # write a heading line, if there isn't already one:
    echo "#ncore" $'\t' "nrank" $'\t' "bm_time" $'\t' "walltime" >> $timing_file
  fi

  # how many mpi per node will fit with this OMP_NUM_THREADS ?
  cpus_per_rank=$(( OMP_NUM_THREADS * hyperthreads_per_core/max_tasks_per_core ))
  max_mpi_per_node=$((cpus_per_node/cpus_per_rank))
  max_mpi_ranks=$((PBS_NUM_NODES * max_mpi_per_node))

  # we need at least 8 cores for each run, since we run up to 8 OMP threads:
  for i in $(seq 3 10) ; do
    nthr=$((2**i)) # 8, 16, 32, 64 ..
    nranks=$((nthr / OMP_NUM_THREADS))
    # use as few nodes as needed for any given run:
    nnodes=$(( (nranks+max_mpi_per_node-1)/max_mpi_per_node))
    [[ $nranks -gt $max_mpi_ranks ]] && break # max we can scale to in this job

    # rather than holding 2 nodes while only using 1, we'll submit this job 
    # separately for each node count, and skip the runs that are not matched 
    # to the current node count:
    [[ $nnodes -lt $PBS_NUM_NODES ]] && continue

    # make the run output easy to identify:
    label=sz${sz}-${PBS_NUM_NODES}n-${nranks}mpi-${OMP_NUM_THREADS}omp-$PBS_JOBID
    # run in a unique directory under $SCRATCH:
    rundir=$SCRATCH/$training_dir/ex7-hybrid/$label
    mkdir -p $rundir
    cd $rundir

    echo "starting miniFE with $nranks MPI processes and $OMP_NUM_THREADS OpenMP threads on $PBS_NUM_NODES nodes at `date`"
    tm=$( { time aprun -n $nranks $cmd > stdout 2> stderr ; } 2>&1 )

    # if the run succeeded, we should have a .yaml file in the $rundir with various
    # metrics about how the run went. We'll extract its report of the total run
    # time, and append a gnuplot data file with the timing info:
    bm_tm=$(awk -F: '/Total Program Time/ { print $2 }' *.yaml)

    ncores=$nthr
    # If you used hyperthreads, then the number of cores is half as many as the 
    # number of hyperthreads:
    #ncores=$((nthr/max_tasks_per_core))
    echo "  $ncores" $'\t' $nranks $'\t' $bm_tm $'\t' $tm >> $timing_file
  done
done
echo "finished miniFE runs at `date`"
